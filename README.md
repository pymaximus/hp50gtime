hp50gtime
=========

A set of programs to enable syncing of calculator time from PC time.

settime.rpl
-----------

A UserRPL program for receiving time from serial port and setting the
calculator time.

hptimesync.py
-------------

A small python program to send time to calculator.


Instructions
------------

0. If needed, install *pyserial* via **pip install pyserial** 
1. Upload settime.rpl via kermit to calculator and then press the
   the settime.rpl softkey to create SYNCT variable.
2. Press SYNCT on calculator
3. Run hptimesync.py /dev/ttyUSB2 on laptop (eg: Fedora VM)

Note: received time string is placed on stack as string
so you can see what was received.


Simple but effective.
