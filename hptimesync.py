#!/usr/bin/env python3
"""This module provides functions to sync calculator time from PC time"""

#
# Simple time sync for HP50G calculator
# Use with settime.rpl (SYNCT)
#
# Copyright (C) 2019 Frank Singleton b17flyboy@gmail.com
#
# 1. Run SYNCT on calculator
# 2. Run this python script ./hptimesync.py /dev/ttyUSB2
#

# standard modules
import datetime
import sys
import time

# 3rd party modules
import serial

# local modules


def do_send_time(port):
    """Send current PC time to calculator in HH.MMSSss format"""
    with serial.Serial(port, 9600, timeout=6) as ser:
        # clear in and out buffers
        time.sleep(0.1)
        ser.reset_input_buffer()
        ser.reset_output_buffer()
        time.sleep(0.1)

        # get PC time, format and send as bytes to calculator
        # this seems to give at least 10msec resolution on Linux

        hp_time_str = datetime.datetime.now().strftime("%H.%M%S%f")[0:9].encode()
        ser.write(hp_time_str)
        print('Sent {}'.format(hp_time_str))

if __name__ == '__main__':
    do_send_time(sys.argv[1])
